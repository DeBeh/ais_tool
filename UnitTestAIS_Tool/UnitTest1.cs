﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;

namespace AIS_Tool
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod_sqlconnection()
        {
            using (SqlConnection connection = new SqlConnection("integrated security=SSPI;data source=(localdb)\\MSSQLLocalDB;persist security info=False;initial catalog=AIS"))
            {
                try
                {
                    connection.Open();
                    Assert.IsTrue(true);
                }
                catch (SqlException)
                {
                    Assert.Fail();
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        [TestMethod]
        public void TestMethod2()
        {
            // 2. Testmethode

            // Bei Fehlschlag des Tests
            Assert.Fail();
            // Bei Erfolg des Tests zB.
            //Assert.AreEqual(2, erg);
        }
    }
}
