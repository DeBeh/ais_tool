﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO.Ports;

//using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
//using System.Windows;
//using System.Windows.Input;
//using System.Windows.Media;
using System.Windows.Media.Animation;
//using System.Windows.Media.Imaging;
//using System.Windows.Navigation;
using MapControl;

namespace AIS_Tool
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            // Very important we set the CacheFolder before doing anything so the
            // MapCanvas knows where to save the downloaded files to.
            TileGenerator.CacheFolder = @"ImageCache";
            TileGenerator.UserAgent = "MyDemoApp";

            InitializeComponent();

            menu.Opacity = 0;
        }

        private void btn_verbindeSQL_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!Methoden.ConnectToSql())
                {
                    MessageBox.Show("Allgemeiner Fehler!", "Fehlermeldung", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                Listen._aktuelleAnzeige = Listen._ais_messages;
                listView.ItemsSource = Listen._aktuelleAnzeige;
                statusbar_SQL_status.Content = "verbunden";
            }
            catch
            {
                // bla
                MessageBox.Show("von Catch: Allgemeiner Fehler!", "Fehlermeldung", MessageBoxButton.OK, MessageBoxImage.Error);
                statusbar_SQL_status.Content = "Fehler";
            }
        }
        private void btn_verbindeAIS_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Hier wurde noch nichts programmiert!");
            statusbar_AIS_status.Content = "Fehler";
        }
        private void tb_Suchfeld_TextChanged(object sender, TextChangedEventArgs e)
        {
            Methoden.Suchen(tb_Suchfeld.Text);
            listView.ItemsSource = Listen._aktuelleAnzeige;
        }
        private void btn_Search_reset_Click(object sender, RoutedEventArgs e)
        {
            tb_Suchfeld.Text = "";
        }

        // Hauptfenster Interaktionen
        private async void Hauptfenster_Loaded(object sender, RoutedEventArgs e)
        {
            Map.Center(54.195313, 9.124300, 9);        //Koordinaten der Fh Westkuetse
            Startup_KeyBinding KeyBinding_window = new Startup_KeyBinding();
            KeyBinding_window.Show();
            await Task.Delay(10000);
            KeyBinding_window.Close();
        }
        private void Hauptfenster_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Application.Current.Shutdown();
            }
            if (e.Key == Key.LeftCtrl)
            {
                //menu.Visibility = Visibility.Visible;
                menu.Opacity = 100;
            }
        }
        private void Hauptfenster_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftCtrl)
            {
                //menu.Visibility = Visibility.Hidden;
                menu.Opacity = 0;
            }
        }
        private void menu_MouseEnter(object sender, MouseEventArgs e)
        {
            menu.Opacity = 100;
        }
        private async void menu_MouseLeave(object sender, MouseEventArgs e)
        {
            await Task.Delay(4000);
            menu.Opacity = 0;
        }
        private void menu_Beenden_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void menu_Tastaturbelegung_Click(object sender, RoutedEventArgs e)
        {
            Startup_KeyBinding KeyBinding_window = new Startup_KeyBinding();
            KeyBinding_window.Show();
        }
        private void menu_Version_Click(object sender, RoutedEventArgs e)
        {
            Version version_window = new Version();
            version_window.Show();
        }

        // Statusbar Mauspositionen (Hauptfenster, Karte)
        private void Hauptfenster_MouseMove(object sender, MouseEventArgs e)
        {
            Point position = Mouse.GetPosition(Hauptfenster);
            statusbar_Maus_Position.Content = position;
        }

        private void btn_map_reset_Click(object sender, RoutedEventArgs e)
        {
            Map.Center(54.195313, 9.124300, 10);        //Koordinaten der Fh Westkuetse
        }

        private void Map_MouseMove(object sender, MouseEventArgs e)
        {
            // Position in Pixel
            Point position = Mouse.GetPosition(Map);
            statusbar_Maus_Position_Map.Content = String.Format("{0:000}", position);

            // Ort in Koordinaten
            Point location = Map.GetLocation(position);
            statusbar_Maus_Ort_Map_Longitude.Content = String.Format("{0:0.000}", location.X);
            statusbar_Maus_Ort_Map_Latitude.Content = String.Format("{0:0.000}", location.Y);




            Point startPoint = e.GetPosition(Map);

            Rectangle rect = new Rectangle();
            rect = new System.Windows.Shapes.Rectangle();
            rect.Stroke = new SolidColorBrush(Colors.Black);
            rect.Fill = new SolidColorBrush(Colors.Black);
            rect.Width = 10;
            rect.Height = 10;

            Point p = Map.GetPoint(54.195313, 9.124300);
            if (p.X == -1)
            {
                //Ausserhalb des sichtbaren Bereichs
            }

            Canvas.SetLeft(rect, p.X);
            Canvas.SetTop(rect, p.Y);

            Map.Children.Add(rect);
        }
    }         
}             
