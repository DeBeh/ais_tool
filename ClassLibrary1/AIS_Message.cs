﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace AIS_Tool
{
    public class AIS_Message    // Klasse, welches ein Paket im AIS-Standard darstellt
    {
        public string ID { get; set; }
        public string Msg_ID { get; set; }
        public string repeat_indicator { get; set; }
        public string User_ID { get; set; }
        public string navigational_status { get; set; }
        public string RoT { get; set; }
        public string Speed { get; set; }
        public string position_acc { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Course { get; set; }
        public string True_Heading { get; set; }
        public string time_stamp { get; set; }
        public string special_manouvre_indicator { get; set; }
        public string spare { get; set; }
        public string RAIM_flag { get; set; }
        public string communication_state { get; set; }
        public Point pixelort { get; set; } // umgerechnete Koordinaten in Pixel
    }
}
