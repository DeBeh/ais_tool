﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS_Tool
{
    static public class Listen  // Klasse, die alle Listen enthält
    {
        static public List<AIS_Message> _ais_messages = new List<AIS_Message>();    //Zeilen der Tabelle
        static public List<Schiff> _Schiffsliste = new List<Schiff>();              //Alle erfassten Schiffe
        static public List<AIS_Message> _aktuelleAnzeige = new List<AIS_Message>();         //nur gefilterte Schiffe    //TEMPORÄR
        //static public List<Schiff> _aktuelleAnzeige = new List<Schiff>();   
    }
}
