﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO.Ports;
using System.Drawing;


namespace AIS_Tool
{
    static public class Methoden   // Klasse, die alle Methoden enthält
    {
        static public bool ConnectToSql()  // baut eine Verbindung zur SQL-DB auf
        {
            SqlConnection conn = new System.Data.SqlClient.SqlConnection();

            conn.ConnectionString = "integrated security=SSPI;data source=(localdb)\\MSSQLLocalDB;persist security info=False;initial catalog=AIS";
            try
            {
                conn.Open();
                string queryString = "SELECT * FROM AIS_data;";
                //MessageBox.Show("Verbinden war erfolgreich.");
                SqlCommand command = new SqlCommand(queryString, conn);

                SqlDataReader reader = command.ExecuteReader();

                //int i = 0;
                while (reader.Read())
                {
                    try
                    {
                        Listen._ais_messages.Add(new AIS_Message()
                        {
                            ID = reader[0].ToString(),
                            Msg_ID = reader[1].ToString(),
                            repeat_indicator = reader[2].ToString(),
                            User_ID = reader[3].ToString(),
                            navigational_status = reader[4].ToString(),
                            RoT = reader[5].ToString(),
                            Speed = reader[6].ToString(),
                            position_acc = reader[7].ToString(),
                            Longitude = reader[8].ToString(),
                            Latitude = reader[9].ToString(),
                            Course = reader[10].ToString(),
                            True_Heading = reader[11].ToString(),
                            time_stamp = reader[12].ToString(),
                            special_manouvre_indicator = reader[13].ToString(),
                            spare = reader[14].ToString(),
                            RAIM_flag = reader[15].ToString(),
                            communication_state = reader[16].ToString()
                        });
                    }
                    catch
                    {
                        //MessageBox.Show("Fehlerhafter Datenbankeintrag in Zeile: " + i, "Fehler!", MessageBoxButton.OK, MessageBoxImage.Error);
                        return false;
                    }
                    //i++;
                }
                //listView.ItemsSource = row;
                reader.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Failed to connect to data source");
                return false;
            }
            finally
            {
                conn.Close();
            }
            return true;
        }

        static public void Suchen(String s)
        {
            Listen._aktuelleAnzeige = new List<AIS_Message>();
            foreach (AIS_Message Message in Listen._ais_messages)
            {
                if (Message.ID.StartsWith(s))
                    Listen._aktuelleAnzeige.Add(Message);
            }
        }

        static public bool ZeichneSchiff() // zeichnet ein Schiff auf die Karte
        {
            
            if(true)    // Wenn Schiff erfolgreich gezeichnet
            {
                return true;
            }
            else // Wenn Schiff nicht gezeichnet wurde
            {
                return false;
            }
        }

        static public List<Schiff> FilterSchiffeZeitraum() // filtert alle Schiffe aus, welche die Bedingungen erfüllen (Zeitraum)
        {
            List<Schiff> Filterung = new List<Schiff>();
            return Filterung;
        }

        static public List<Schiff> FilterSchiffeAuswahl()  // filtert alle Schiffe aus, welche die Bedingungen erfüllen (Auswahl)
        {
            List<Schiff> Filterung = new List<Schiff>();
            return Filterung;
        }
    }
}
