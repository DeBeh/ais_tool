﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS_Tool
{
    public class Schiff         // Klasse, welches ein Schiff darstellt
    {
        public List<AIS_Message> _Chronik = new List<AIS_Message>();    // Liste, die alle Messages für ein Schiff enthält
        public Schiff() // Konstruktor
        {

        }
        public bool ZeigeSchiff()
        {
            if (Methoden.ZeichneSchiff() == true)
                return true;
            else
                return false;
        }
    }
}
